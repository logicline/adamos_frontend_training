# Adamos training app
* This app was created by creating a tutorial app from website 
https://cumulocity.com/guides/web/angular/
* This repo contains two commits, one initial commit with the initial tutorial state, 
and another one with a component added that reads all measurements from the realtime 
api

# Some useful commands
1. clone the repo
2. cd into the repo with `cd adamos_frontend_training` and run `npm install`
3. then run `c8ycli serve -u http://{your-tenant}.adamos-dev.com` to start the local 
dev server and follow the cli instructions to open the app
4. when on master, use the command `git diff -w ec95d7b` to see the changes that have 
been made to create the 
additional UI component to read realtime measurements



Maintainer: Mario Hammer

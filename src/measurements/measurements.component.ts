import { _ } from '@c8y/ngx-components';
import { Component } from '@angular/core';
import { IMeasurement, Realtime } from '@c8y/client';

@Component({
    selector: 'measurements',
    templateUrl: './measurements.component.html'
})
export class MeasurementsComponent {
    measurements: IMeasurement[] = [];

    constructor(private realtime: Realtime) {
        // _ annotation to mark this string as translatable string.
        const subscription = realtime.subscribe('/measurements/*', (msg) => {
            console.log(msg); // logs all alarm CRUD changes
            this.measurements.push(msg.data.data);
        });
    }
}
